package com.siteowl.tests;

import org.testng.annotations.Test;

import com.cipl.automation.property.PropertyManager;
import com.cipl.utility.Reporter;
import com.cipl.utility.Reporter.MessageType;
import com.siteowl.pages.DashboardPage;
import com.siteowl.pages.LoginPage;

public class LoginTest {


	/*
	 * @author Priyanka Deb
	 * Date: 08.07.2019
	 * 
	 * <liOpen the URL of the application into browser.</li>
	 * <li>Click on "Login" button without entering Email and password.</li>
	 * <li> Click on "Login" button after entering space in Email address and password</li>
	 * <li>  Enter invalid email address, password and click on "Login" button</li>
	 * <li>Enter invalid email address, valid password and click on "Login" button</li>
	 * <li>Enter valid email address, invalid password and click on "Login" button</li>
	 * <li>Enter valid email address, password and click on "Login" button</li>
	 */
	
	@Test(description = "Login screen")
	public void C402() {

		// Step 1 - Open the URL of the application into browser.
		Reporter.log("Step 1 - Open the URL of the application into browser.", MessageType.MESSAGE);
		LoginPage loginPage = new LoginPage();
		DashboardPage dashboardPage=new DashboardPage();
		loginPage.navigateToPage();

		// Step 2 - Click on "Login" button without entering Email and password.
		Reporter.log("Step 2 - Click on \"Login\" button without entering Email and password.", MessageType.MESSAGE);
		loginPage.doLogin("", "");
		loginPage.getTxtUsername().getMessage()
				.verifyText(PropertyManager.getResourceBundle().getString("loginpage.email.requied.message"));
		loginPage.getTxtPassword().getMessage().verifyText(PropertyManager.getResourceBundle().
				getString("loginpage.password.requied.message"));

		// Step 3 - Click on "Login" button after entering space in Email address and
		// password.
		Reporter.log("Step 3 - Click on Login button after entering space in Email address and Password", MessageType.MESSAGE);
		loginPage.doLogin(" ", " ");
		loginPage.getEmailInvalidMssg()
				.verifyText(PropertyManager.getResourceBundle().getString("loginpage.invalidemail.required.message"));
		

		// Step 4 - Enter invalid email address, password and click on "Login" button.
		Reporter.log("Step 4 - Enter invalid email address, password and click on Login button.", MessageType.MESSAGE);
		loginPage.doLogin(PropertyManager.getResourceBundle().getString("invalidusername"),
				PropertyManager.getResourceBundle().getString("invalidpassword"));
		loginPage.getEmailInvalidMssg()
		.verifyText(PropertyManager.getResourceBundle().getString("loginpage.invalidemail.required.message"));
		
		// Step 5 - Enter invalid email address, valid password and click on "Login" button.
		Reporter.log("Step 5-Enter invalid email address, valid password and click on Login button.", MessageType.MESSAGE);
		loginPage.doLogin(PropertyManager.getResourceBundle().getString("invalidusername"),
				PropertyManager.getResourceBundle().getString("validPwd"));
		loginPage.getTxtUsername().getMessage()
		.verifyText(PropertyManager.getResourceBundle().getString("loginpage.invalidemail.required.message"));
		
		
		// Step 6 - Enter valid email address, invalid password and click on "Login" button. 
		Reporter.log("Step 6-Enter valid email address, invalid password and click on Login button.", MessageType.MESSAGE);
		loginPage.doLogin(PropertyManager.getResourceBundle().getString("validUsername"),
				PropertyManager.getResourceBundle().getString("invalidpassword"));
		loginPage.getInvalidMssg()
		.verifyText(PropertyManager.getResourceBundle().getString("loginpage.invalidemailpwd.required.message"));
		
		// Step 7 - Enter valid email address, password and click on "Login" button.
		Reporter.log("Step 7 - Enter valid email address, password and click on \"Login\" button..", MessageType.MESSAGE);
		loginPage.doLogin();
		dashboardPage.waitForPageToLoad();
	}
}
