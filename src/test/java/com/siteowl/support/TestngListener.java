package com.siteowl.support;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.cipl.webdriver.TestBase;

/*
 * @author Priyanka Deb
 * Date->05.07.2019
 * Listener to perform specific operations on starting and finishing execution
 */
public class TestngListener implements ISuiteListener {

	@Override
	public void onStart(ISuite suite) {
		TestBase.getDriver().manage().window().maximize();
	}

	@Override
	public void onFinish(ISuite suite) {
		TestBase.getDriver().quit();
	}
}
