package com.siteowl.locator;

/*
 *@author Priyanka Deb
 *Date->05.07.2019
 * Locators of Webelements of Login Page 
 */


public interface Locators {

	public interface component {
		public interface TextBoxComponentLocators {
			String LABEL_USERNAME_LOC = "css=label";
			String INPUTBOX_USERNAME_LOC = "css=input";
			String VALIDATIONMESSAGE_LOC = "css=control-messages";
		}
	}

	public interface pages {

		public interface loginpageLocators {

			String INPUTBOX_USERNAME_LOC = "xpath=//div[@class='form-group'][1]";
			String INPUTBOX_PASSWORD_LOC = "xpath=//div[@class='form-group'][2]";
			String BTN_LOGIN_LOC = "xpath=//button";
			String MSG_ERR_UNAME_LOC = "xpath=//div[contains(text(),'Email Address is required')]";
			String MSG_ERR_PASSWORD_LOC = "xpath=//div[contains(text(),'Password is required')]";
			String LINK_RESET_PASSWORD_LOC = "xpath=//a[@title='Reset Password']";
			String MSG_INVALID_EMAIL_LOC = "xpath=//div[contains(text(),'Invalid email address')]";
			String MSG_EMAILORPASS_LOC = "xpath=//div/p[contains(text(),'Invalid Email or Password')]";
			String BTN_DRP_LOGOUT_LOC = "xpath=//div[contains(@class,'user-dropdown')]//a[contains(@class,'btn-dropdown')]";
		}

		public interface DashboardPageLocators {
			String LABEL_HEADER_LOC = "xpath=//h1[contains(text(),' Regression Special')]";
		}
	}
}
