package com.siteowl.pages;

import com.cipl.annotation.FindBy;
import com.cipl.webdriver.TWWebdriverBaseTestPage;
import com.cipl.webelement.TWWebElement;
import com.siteowl.locator.Locators.pages.DashboardPageLocators;

/*
 * @author Priyanka Deb
 * Date->05.07.2019
 * 
 * This class is for Dashboard page
 * Navigate to  Dashboard-> All the elements and methods on site listing page
 */
public class DashboardPage extends TWWebdriverBaseTestPage<LoginPage> implements DashboardPageLocators {

	@FindBy(locator = LABEL_HEADER_LOC, name = "Header Label")
	private TWWebElement lblHeader;

	@Override
	public boolean isPageOpen(Object... Args) {
		return lblHeader.isPresent() && lblHeader.isDisplayed();
	}

	@Override
	protected void openPage(Object... Args) {
		parent.doLogin();
	}

	@Override
	public void waitForPageToLoad() {
		lblHeader.waitForPresent();
	}
}