package com.siteowl.pages;

import com.cipl.annotation.FindBy;
import com.cipl.automation.property.PropertyManager;
import com.cipl.webdriver.TWWebdriverBaseTestPage;
import com.cipl.webdriver.TWWebdriverTestPage;
import com.cipl.webdriver.TestBase;
import com.cipl.webelement.TWWebElement;
import com.siteowl.component.SiteOwlTextBox;
import com.siteowl.locator.Locators.pages.loginpageLocators;

/*
 * @author Priyanka Deb
 * Date->05.07.2019
 * 
 * This class is for Login page
 * Open URL of site and Login page will be open -> All the elements and methods on site listing page
 */
public class LoginPage extends TWWebdriverBaseTestPage<TWWebdriverTestPage> implements loginpageLocators {

	@FindBy(locator = INPUTBOX_USERNAME_LOC, name = "username")
	private SiteOwlTextBox txtUsername;

	@FindBy(locator = INPUTBOX_PASSWORD_LOC, name = "password")
	private SiteOwlTextBox txtPassword;

	@FindBy(locator = BTN_LOGIN_LOC, name = "loginbtn")
	private TWWebElement btnLogin;

	@FindBy(locator = MSG_EMAILORPASS_LOC, name = "invalid mssg")
	private TWWebElement invalidMssg;

	@FindBy(locator = MSG_INVALID_EMAIL_LOC, name = "email invalid mssg")
	private TWWebElement emailInvalidMssg;

	public SiteOwlTextBox getTxtPassword() {
		return txtPassword;
	}

	public SiteOwlTextBox getTxtUsername() {
		return txtUsername;
	}

	public TWWebElement getBtnLogin() {
		return btnLogin;
	}

	public TWWebElement getInvalidMssg() {
		return invalidMssg;
	}

	public TWWebElement getEmailInvalidMssg() {
		return emailInvalidMssg;
	}

	/*
	 * Verify UserEditBox and Password is Present or not
	 */
	@Override
	public boolean isPageOpen(Object... Args) {
		return txtUsername.isPresent() && txtPassword.isPresent();
	}

	/*
	 * Delete all cookies and Navigate to login page
	 */
	@Override
	protected void openPage(Object... Args) {
		TestBase.getDriver().manage().deleteAllCookies();
		TestBase.getDriver().get("https://staging.site-owl.com/public/login");
	}

	/*
	 * Wait till loginpage get loaded
	 */
	@Override
	public void waitForPageToLoad() {
		txtUsername.waitForPresent();
		txtPassword.waitForPresent();
	}

	/*
	 * @author Priyanka Deb 1)Enter valid username and password 2)Click on Login
	 * 
	 * 
	 */
	public void doLogin() {
		txtUsername.sendKeys(PropertyManager.getResourceBundle().getString("validUsername"));
		txtPassword.sendKeys(PropertyManager.getResourceBundle().getString("validPwd"));
		btnLogin.click();
	}

	/*
	 * @author Priyanka Deb 1)Enter username and password 2)Click on Login
	 * 
	 */
	public void doLogin(String Username, String Password) {
		txtUsername.sendKeys(Username);
		txtPassword.sendKeys(Password);
		btnLogin.click();
	}
}
