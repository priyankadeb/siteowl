package com.siteowl.component;

import com.cipl.annotation.FindBy;
import com.cipl.webelement.TWComponent;
import com.cipl.webelement.TWRemoteWebElement;
import com.cipl.webelement.TWWebElement;
import com.siteowl.locator.Locators.component.TextBoxComponentLocators;

/*
 * @author Priyanka Deb
 * Date: 08.07.2019
 * Locators and Methods of components which is common to all pages
 * 
 */
public class SiteOwlTextBox extends TWComponent implements TextBoxComponentLocators {

	@FindBy(locator = LABEL_USERNAME_LOC, name = "Label")
	private TWWebElement label;

	@FindBy(locator = INPUTBOX_USERNAME_LOC, name = "Input Box")
	private TWWebElement input;

	@FindBy(locator = VALIDATIONMESSAGE_LOC, name = "Validation Message")
	private TWWebElement message;
	
	public TWWebElement getLabel() {
		return label;
	}
	
	public TWWebElement getInput() {
		return input;
	}
	
	public TWWebElement getMessage() {
		return message;
	}

	public SiteOwlTextBox(String locator, String name) {
		super(locator, name);
	}

	public SiteOwlTextBox(TWRemoteWebElement parentElement, String locator, String name) {
		super(parentElement, locator, name);		
	}
	
	@Override
	public void sendKeys(CharSequence... keysToSend) {
		input.clear();
		input.sendKeys(keysToSend);
	}	
}